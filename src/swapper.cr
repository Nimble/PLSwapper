# Actual swaping of playlist from one format to another
# Different cases for different hosts
# Make use of the metadata class for concurrency
class Swapper
extend Reader_Input
  reader = Reader_Input.cli_input
  puts "The Beatles are singing: #{"🎵#{reader.lyrics}🎶🎸🥁".colorize.mode(:blink)}"
end
